# Download this repository

```
git clone https://gitlab.com/jesse.devore/cppdevenvironment.git
```

# Install Visual Studio, WSL, Ubuntu (WSL)

Open PowerShell with administrative privileges and enter the command:

```
set-executionpolicy bypass -scope process; install_base.ps1
```

To also install additional useful tools (chocolately, notepad++, MobaXTerm), also execute the following in the same terminal:

```
set-executionpolicy bypass -scope process; install_tools.ps1
```

# Install VMware or VirtualBox (Pick your favorite)

[https://www.vmware.com/products/workstation-pro.html]

[https://www.vmware.com/products/workstation-player.html]

[https://download.virtualbox.org/virtualbox/6.0.6/VirtualBox-6.0.6-130049-Win.exe]


# Setup a Docker CE virtual machine

Download the Ubuntu 18.04 ISO from [https://www.ubuntu.com/download/server]

Create a new VM and give it al least 100GB of drive space and 4096M of RAM. This is going to house all the docker images and will run the docker daemon. Once the install is complete, run the setup script:

During the install, don't include any extra packages except for openssh-server. Also, don't turn off SSH password authentication. That can be done later. It's used during the initialization process.

> Virtual Box Users: Change the network adapter to **Bridged**

Log in to the VM once it's ready and execute the following at a shell prompt:

```
$ sudo useradd -m -p $(openssl passwd -1 temporary) setup
$ sudo usermod -aG sudo setup
$ ifconfig
```

Take note of the IP address. Next, open an Ubuntu console. Copy the git repo. Edit the /etc/hosts file and add an entry for docker.local with the Docker VMs IP address.

```
$ chmod +x *.sh
$ ./setup_docker.sh [ip address]
```

Test that the docker connection works:

```
$ docker info
```

Log on directly to the Docker VM and delete the setup user:

```
$ sudo deluser --remove-home setup
```

Go back to PowerShell and update the hosts file using the add_host script:

```
set-executionpolicy bypass -scope process; add_host.ps1 [ip address]
```


# Build docker images

From the Ubuntu console, build the base build images:

```
$ ./build-gcc_x86_64_base.sh 
```

Edit gcc_x86_64_with_libs.dockerfile and add any additional libraries you will need. Once it's ready, build it.

```
$ docker build -t gcc_x86_64:with_libs -f gcc_x86_64_with_libs.dockerfile
```

# Launch build container

```
$ ./start_gcc_x86_64.sh
```

# Configure Visual Studio

In Visual Studio, open the Tools menu and click Options. Under the Cross Platform section, there is a Connection Manager. That is where connections to remote machines can be configured. Click Add. Enter docker.local for the host name, root for user name, and open the id_build_user for the private key. 

# Setup Git repository connection

TODO


