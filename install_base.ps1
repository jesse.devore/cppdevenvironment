try
{
	Get-Command choco.exe
}
Catch
{
	Set-ExecutionPolicy AllSigned -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

# Visual studio 2019 CE plus workload
choco install -y visualstudio2019community
choco install -y visualstudio2019-workload-nativecrossplat

# WSL Ubuntu 18.04 (and make sure WSL is there)
choco install -y wsl
choco install -y wsl-ubuntu-1804
