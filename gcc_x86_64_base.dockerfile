FROM ubuntu:18.04

RUN apt-get update && apt-get install -y g++ make openssh-server rsync

# Grab a recent version of CMAKE (the repository version is too out of date)
RUN wget -qO- "https://cmake.org/files/v3.14/cmake-3.14.2-Linux-x86_64.tar.gz" | tar --strip-components=1 -xz -C /usr/local

RUN mkdir /var/run/sshd

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# Copy keys for SSH host id and login
COPY id_build_user.pub /root/.ssh/authorized_keys
COPY ssh_host_rsa_key /etc/ssh/
COPY ssh_host_rsa_key.pub /etc/ssh/
COPY gdb /usr/bin/
COPY libreadline.so.7 /lib/x86_64-linux-gnu/
COPY libpython3.6m.so.1.0 /usr/lib/x86_64-linux-gnu/
COPY libmpfr.so.6 /usr/lib/x86_64-linux-gnu/
COPY libbabeltrace.so.1 /usr/lib/x86_64-linux-gnu/
COPY libbabeltrace-ctf.so.1 /usr/lib/x86_64-linux-gnu/


# Need port 22 for SSH. Expose 8080 for testing network apps.
# Don't forget to map the port when running -ip localport:22
EXPOSE 22
EXPOSE 8080

CMD ["/usr/sbin/sshd", "-D"]
