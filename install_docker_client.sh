#!/bin/sh

# Download and add Docker verify key
curl -fssL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

# Add the Docker repository to apt
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Install Docker CE client
apt update && apt install -y docker-ce-cli
