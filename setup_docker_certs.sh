#!/bin/sh

# Generate CA key and cert
openssl genrsa -out ca-key.pem 4096
openssl req -new -x509 -days 3650 -key ca-key.pem -sha256 -out ca.pem -subj /CN=docker-ca

IP=$1
HOST="docker.local"

# Generate host key and cert
openssl genrsa -out server-key.pem 4096
openssl req -subj "/CN=$HOST" -new -key server-key.pem -out server.csr

echo subjectAltName = DNS:$HOST,IP:$IP,IP:127.0.0.1 >> extfile.cnf
echo extendedKeyUsage = serverAuth >> extfile.cnf

# Sign the server cert
openssl x509 -req -days 3650 -sha256 -in server.csr -CA ca.pem -CAkey ca-key.pem \
          -CAcreateserial -out server-cert.pem -extfile extfile.cnf

# Now for the client
openssl genrsa -out docker-client-key.pem 4096
openssl req -subj '/CN=docker-client' -new -key docker-client-key.pem -out docker-client.csr
echo extendedKeyUsage = clientAuth > extfile-client.cnf
openssl x509 -req -days 3650 -sha256 -in docker-client.csr -CA ca.pem -CAkey ca-key.pem \
          -CAcreateserial -out docker-client-cert.pem -extfile extfile-client.cnf

# Update permissions to be more secure
chmod -v 0400 ca-key.pem docker-client-key.pem server-key.pem
chmod -v 0444 ca.pem server-cert.pem docker-client-cert.pem

# Clean up. We don't need to keep the signing requests or cnfs
rm -v docker-client.csr server.csr extfile.cnf extfile-client.cnf

# Move client cert and key to docker config directory
mkdir -pv ~/.docker
cp ca.pem ~/.docker
mv docker-client-cert.pem ~/.docker/cert.pem
mv docker-client-key.pem ~/.docker/key.pem
