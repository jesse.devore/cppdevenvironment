#!/bin/sh

# Download and add Docker verify key
curl -fssL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

# Add the Docker repository to apt
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Install Docker CE
apt update && apt install -y docker-ce

# Move Docker options file
mkdir -p /etc/systemd/system/docker.service.d
mv options.conf /etc/systemd/system/docker.service.d/

# Move Docker key and certs
mkdir -p /var/lib/docker/certs
mv ca.pem /var/lib/docker/certs
mv server-key.pem /var/lib/docker/certs
mv server-cert.pem /var/lib/docker/certs

# Restart Docker with new configuration
systemctl daemon-reload
systemctl restart docker
