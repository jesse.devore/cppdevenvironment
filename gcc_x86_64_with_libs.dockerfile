FROM gcc_x86_64:base

# Get and install Boost
RUN wget -qO- https://dl.bintray.com/boostorg/release/1.70.0/source/boost_1_70_0.tar.bz2 | tar -xj
RUN cd boost_1_70_0 && ./bootstrap.sh && ./bjam install && cd ../ && rm -rf boost_1_70_0

# Install Perl FindBin module as prerequisite to OpenSSL build
RUN apt-get install libfindbin-libs-perl -y

# Get and install OpenSSL
RUN wget -qO- https://www.openssl.org/source/openssl-1.1.1b.tar.gz | tar -xz
RUN cd openssl-1.1.1b && ./config && make && make install && cd ../ && rm -rf openssl-1.1.1b
