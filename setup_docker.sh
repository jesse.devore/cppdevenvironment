#!/bin/sh
IP=$1
./setup_docker_certs.sh $IP
sudo apt install sshpass -y
ssh-keyscan -H $IP >> ~/.ssh/known_hosts
sshpass -p temporary scp ca.pem server-key.pem server-cert.pem install_docker_ce.sh options.conf setup@$IP:~
sshpass -p temporary ssh setup@$IP "echo temporary | sudo -S -k ./install_docker_ce.sh"

# Tell docker client where to look and to use buildkit
echo "export DOCKER_HOST=tcp://docker.local:2376 DOCKER_TLS_VERIFY=1" >> ~/.bashrc
echo "export DOCKER_BUILDKIT=1" >> ~/.bashrc