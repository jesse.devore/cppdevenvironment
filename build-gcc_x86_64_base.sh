#!/bin/sh

# Generate SSH login key if it doesn't already exist
if [ ! -f id_build_user ]; then
    ssh-keygen -t rsa -b 4096 -f id_build_user -q -N ""
fi

# Start setting up the docker build context directory
mkdir context 2>/dev/null
cp id_build_user.pub context/

# Grab the special Microsoft build of GDB from WSL to add to the image
cp /usr/bin/gdb context/
cp /lib/x86_64-linux-gnu/libreadline.so.7 context/
cp /usr/lib/x86_64-linux-gnu/libpython3.6m.so.1.0 context/
cp /usr/lib/x86_64-linux-gnu/libmpfr.so.6 context/
cp /usr/lib/x86_64-linux-gnu/libbabeltrace.so.1 context/
cp /usr/lib/x86_64-linux-gnu/libbabeltrace-ctf.so.1 context/


# Generate SSH host key if it doesn't already exist
if [ ! -f context/ssh_host_rsa_key ]; then
    ssh-keygen -t rsa -b 4096 -f context/ssh_host_rsa_key -q -N ""
fi

docker build -t gcc_x86_64:base -f gcc_x86_64_base.dockerfile ./context

WIN_HOME=$(wslpath `cmd.exe /c echo "%userprofile%" | sed -e 's/\r$//'`)
cp id_build_user $WIN_HOME
